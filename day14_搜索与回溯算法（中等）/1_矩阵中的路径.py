class Solution:
    def exist(self, board: List[List[str]], word: str) -> bool:
        def dfs(i, j, k):
            # 索引越界或者不匹配，返回False
            if not 0 <= i < len(board) or not 0 <= j < len(board[0]) or board[i][j] != word[k]:
                return False
            if k == len(word) - 1:  return True # 匹配到word末尾，返回True
            board[i][j] = ''    # 标记匹配过的字母，防止往回走
            # 四个方向有一个成功就成功
            res = dfs(i+1, j, k+1) or dfs(i-1, j, k+1) or dfs(i, j+1, k+1) or dfs(i, j-1, k+1)
            board[i][j] = word[k]   # 还原被标记的字母，因为这次没有匹配成功，下一次还要继续匹配
            return res
        
        for i in range(len(board)):
            for j in range(len(board[0])):
                if dfs(i, j, 0):    return True
        return False
