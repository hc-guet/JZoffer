# 解法一
class Solution:
    def firstUniqChar(self, s: str) -> str:
        dic, n = {}, len(s)
        for i, c in enumerate(s):
            dic[c] = i if c not in dic else -1
        first = n
        for pos in dic.values():
            if pos != -1 and pos < first:
                first = pos
        return ' ' if first == n else s[first]

# 解法二
class Solution:
    def firstUniqChar(self, s: str) -> str:
        dic = {}
        for c in s:
            dic[c] = not c in dic    
        for c in s:
            if dic[c]:  return c
        return ' '

# 解法三
class Solution:
    def firstUniqChar(self, s: str) -> str:
        dic = {}
        for c in s:
            dic[c] = not c in dic
        # Python 3.6 之后，默认字典有序    
        for k, v in dic.items():
            if v:  return k
        return ' '



