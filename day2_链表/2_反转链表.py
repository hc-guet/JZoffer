# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def reverseList(self, head: ListNode) -> ListNode:
        p, nxt = None, None
        while head:
            nxt = head.next
            head.next = p
            p = head
            head = nxt
        return p