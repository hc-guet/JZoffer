class Solution:
    def replaceSpace(self, s: str) -> str:
        res = ''
        for i in s:
            if i == ' ':
                res = res + '%20'
            else:
                res = res + i
        return res