# 这是我自己写的暴力解法
class MedianFinder:

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.obj = []


    def addNum(self, num: int) -> None:
        self.obj.append(num)


    def findMedian(self) -> float:
        if not self.obj:
            return None
        self.obj.sort()
        l = len(self.obj)
        if l % 2 == 0:
            return (self.obj[l//2-1]+self.obj[l//2])/2
        else:
            return self.obj[l//2]

# 堆
class MedianFinder:

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.A = [] # 小顶堆，保存较大的一半
        self.B = [] # 大顶堆，保存较小的一半


    def addNum(self, num: int) -> None:
        if len(self.A) != len(self.B):
            heappush(self.A, num)
            heappush(self.B, -heappop(self.A))
        else:
            heappush(self.B, -num)
            heappush(self.A, -heappop(self.B))


    def findMedian(self) -> float:
        return self.A[0] if len(self.A) != len(self.B) else (self.A[0] - self.B[0]) / 2.0



# Your MedianFinder object will be instantiated and called as such:
# obj = MedianFinder()
# obj.addNum(num)
# param_2 = obj.findMedian()
