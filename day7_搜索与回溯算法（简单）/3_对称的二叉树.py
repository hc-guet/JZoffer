# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

# 递归
class Solution:
    def isSymmetric(self, root: TreeNode) -> bool:
        def recur(L, R) -> bool:
            if not L and not R: return True
            elif not L or not R or L.val != R.val:  return False
            return recur(L.right, R.left) and recur(L.left, R.right)
        return recur(root.left, root.right) if root else True