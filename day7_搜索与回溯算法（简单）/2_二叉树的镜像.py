# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

# 解法一-辅助栈
class Solution:
    def mirrorTree(self, root: TreeNode) -> TreeNode:
        if not root:    return
        stack = [root]
        while stack:
            node = stack.pop()
            if node.left:   stack.append(node.left)
            if node.right:  stack.append(node.right)
            node.left, node.right = node.right, node.left
        return root
        
# 解法二-递归 比较耗时
class Solution:
    def mirrorTree(self, root: TreeNode) -> TreeNode:
        if not root:    return
        root.left, root.right = self.mirrorTree(root.right), self.mirrorTree(root.left)
        return root