# 转移方程：profit=max(profit,prices[i]−min(cost,prices[i]))
class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        cost, profit = float("+inf"), 0
        for price in prices:
            cost = min(cost, price) # 保存最低价格
            profit = max(profit, price - min(cost, price))
        return profit