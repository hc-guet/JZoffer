class Solution:
    def exchange(self, nums: List[int]) -> List[int]:
        i, j = 0, len(nums)-1
        while i < j :
            while i < j and nums[i] & 1 == 1:   i += 1
            while i < j and nums[j] & 1 == 0:   j -= 1
            nums[i], nums[j] = nums[j], nums[i]
        return nums

# 这题使用下面单指针遍历，时间更少，但是消耗内存多
class Solution:
    def exchange(self, nums: List[int]) -> List[int]:
        odd, even = [], []
        for i in nums:
            if i % 2 == 0:  even.append(i)
            else:   odd.append(i)
        return odd+even
