class Solution:
    def reverseWords(self, s: str) -> str:
        s = s.strip()   # 删除首尾空格
        i = j = len(s) - 1
        res = []
        while i >= 0:
            # 搜索首个空格
            while i >= 0 and s[i] != ' ':   i -= 1
            res.append(s[i+1:j+1])  # 添加单词
            while s[i] == ' ':  i -= 1  # 跳过单词间空格
            j = i   # 指向下个单词的尾字符
        return ' '.join(res)
