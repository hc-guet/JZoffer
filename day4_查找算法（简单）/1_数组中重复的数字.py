# 解法一：字典-我自己想的，感觉不太好
class Solution:
    def findRepeatNumber(self, nums: List[int]) -> int:
        dic = {}
        for i in range(len(nums)):
            if dic.get(nums[i]) != None:
                return nums[i]
            else:
                dic[nums[i]] = i

# 解法二：集合
class Solution:
    def findRepeatNumber(self, nums: List[int]) -> int:
        dic = set()
        for n in nums:
            if n in dic:    return n
            dic.add(n)
        return -1