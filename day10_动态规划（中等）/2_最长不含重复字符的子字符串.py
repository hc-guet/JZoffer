# 解法一-动态规划+哈希表
class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        dic = {}
        res = tmp = 0
        for j in range(len(s)):
            i = dic.get(s[j], -1)   # 获取索引i
            dic[s[j]] = j   # 更新哈希表
            tmp = tmp+1 if tmp < j-i else j-i   # dp[j-1]->dp[j]
            res = max(res, tmp)
        return res

# 解法二-双指针+哈希表
class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        dic, res, i = {}, 0, -1
        for j in range(len(s)):
            if s[j] in dic:
                i = max(dic[s[j]], i)   # 更新左指针i
            dic[s[j]] = j   # 更新哈希表
            res = max(res, j-i)
        return res
