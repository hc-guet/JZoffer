# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        if p.val > q.val:   p, q = q, p # 保证 p.val < q.val
        while root:
            if root.val < p.val:    # p,q都在root的右子树中
                root = root.right   # 遍历至右子节点
            elif root.val > q.val:  # p,q都在root的左子树中
                root = root.left    # 遍历至左子节点
            else:   break
        return root
